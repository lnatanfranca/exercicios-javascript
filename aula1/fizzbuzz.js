const fizzBuzz = function(num){
    const divByThree = num % 3 === 0;
    const divByFive = num % 5 === 0;
    if (divByThree && divByFive) {
        console.log("fizzbuzz");
    } else if (divByThree){
        console.log("fizz");
    } else if (divByFive){
        console.log("buzz");
    } else {
        console.log(num);
    }
}

fizzBuzz(13);
fizzBuzz(24);
fizzBuzz(50);
fizzBuzz(15);
fizzBuzz(0);