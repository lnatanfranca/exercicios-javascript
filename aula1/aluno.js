const calcMedia = function(notaA, notaB, notaC){
    return (notaA + notaB + notaC) / 3;
}

const aprovaAluno = function(notaA, notaB, notaC) {
    const media = calcMedia(notaA, notaB, notaC);
    const mensagemAprova = media >= 6 ? 'Aprovado' : 'Reprovado';
    console.log(mensagemAprova);
}

aprovaAluno(5, 6, 8);
aprovaAluno(10, 10, 10);
aprovaAluno(2, 1, 4);