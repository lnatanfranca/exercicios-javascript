const calcTemp = function(tempFahr){
    const tempCelsius = ((tempFahr - 32) * 5/9).toFixed(2); 
    const mensagem = `${tempFahr}°F = ${tempCelsius}°C`;
    console.log(mensagem);
}

calcTemp(0);
calcTemp(32);
calcTemp(-50.98);